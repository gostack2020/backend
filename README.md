# Pontos importantes sobre o projeto

- Não vamos gerar a build com o comando 'tsc' no ambiente de dev.
- No ambiente dev vamos usar a extensão ts-node para executar nossa aplicação.
  - Ela executa o nodemon que fica assistindo qualquer mudança no código para reiniciar o servidor. E já faz o papel do tsc, que é o de converter o código typescript para javascript puro, o qual é entendível pelo browser.
  - --transpileOnly: não vai verificar se há algo errado no nosso código. Vamos deixar as verificações para o próprio vsCode.
  - --ignore-watch node_modules: evitar que o nosso ts-node-dev tente compilar o código do node_modules.

# Debug no vsCode

- Utilizar a flag inspect junto do ts-node-dev.
- Criar o arquivo launch.json e modificar a propriedade 'restart':'attach'. Dessa forma o debug só será iniciado quando o dev desejar.
